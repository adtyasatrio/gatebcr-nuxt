// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      apiBase: process.env.API_URL || "http://192.168.86.29:6001",
    }
  },
  devtools: { enabled: true },
  modules: ['@nuxtjs/tailwindcss'],
})
